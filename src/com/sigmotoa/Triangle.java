package com.sigmotoa;

public class Triangle extends Geometric {
    public Triangle(String figurename, double high, double sidea, double sideb) {
        super(figurename, sidesnumber, side);
    }
    public Triangle(String figurename, double sidea, double sideb, double sidec) {
        super(figurename, sidesnumber, side);
    }
    public Triangle(String figurename, double side) {
        super(figurename, sidesnumber, side);
    }
    public Triangle(String figurename) {
        super(figurename, sidesnumber, side);
    }
    @Override
    public void areaCalculation()
    {
        area = (this.sidea*this.high)/2;

    }

    @Override
    public void perimeterCalculation()
    {
        perimeter=this.side*3;
    }

    @Override
    public void perimeterCalculationTwo()
    {
        perimeter=(this.side+this.side+this.side);
    }

    @Override
    public double hypotenuseCalculation(double sidea, double sideb){
        double hypot = (sidea*sidea)+(sideb*sideb);
        hypot = Math.sqrt(hypot);
        return hypot;
    }
}


